const fs = require('fs');
const fsPromise = fs.promises;
const filesDir = './files';

if (!fs.existsSync(filesDir)){
  fs.mkdirSync(filesDir);
}

// POST FILE: /api/files
const createFile = async (req, res) => {
  const {filename, content} = await req.body;
  const regExp = /^.*\.(log|txt|json|yaml|xml|js)$/;
  
  if(!regExp.test(filename) || !filename) {
    return res.status(400).json({ message: "Please specify 'filename' parameter"});
  }

  if(!content) {
    return res.status(400).json({ message: "Please specify 'content' parameter" });
  }

  try {
    await fsPromise.writeFile(`./files/${filename}`, content, 'utf-8');
    res.status(200).json({ message: 'File created successfully' });
  } catch(error) {
    res.status(500).json({ message: 'Server error' });
  }
}

// GET ALL FILES: /api/files
const getFiles = async (req, res) => {
  const filesArray = await fsPromise.readdir('./files');
  
  try {
    res.status(200).json({ message: 'Success', files: filesArray });
  } catch (error) {
    res.status(500).json({ message: 'Server error' });
  }
}

//  GET ONE FILE BY NAME: /api/files/:filename
const getFile = async (req, res) => {
  const filename = req.params.filename;
  const fileExt = filename.split('.').pop();

  if (!fs.existsSync(`./files/${filename}`)) {
    return res.status(400).json({ message: `No file with ${filename} filename found` });
  }

  try {
    const { birthtime } = await fsPromise.stat(`./files/${filename}`);
    const content = await fsPromise.readFile(`./files/${filename}`, 'utf-8');

    res.status(200).json({ 
      message: 'Success',
      filename: filename,
      content: content, 
      extension: fileExt,
      uploadedDate: birthtime
     });
  } catch (error) {
    res.status(500).json({ message: 'Server error' });   
  }
}

// PUT FILE BY NAME: /api/files/:filename
const modifyFile = async (req, res) => {
  const filename = req.params.filename;
  const {content} = req.body;

  try {
    await fsPromise.writeFile(`./files/${filename}`, content, 'utf-8');
    res.status(200).json({ message: 'File updated successfully' });
  } catch (error) {
    res.status(500).json({ message: 'Server error' });
  }

}

// DELETE FILE BY NAME: /api/files/:filename
const deleteFile = async (req, res) => {
  const filename = req.params.filename;
  const filesArray = await fsPromise.readdir('./files');
  if(filesArray.find( name => name === filename) === filename) {
    fsPromise.unlink(`./files/${filename}`);
    res.status(200).json({ message: 'File has been deleted successfully' });
  } else {
    res.status(400).json({ message: `File with ${filename} filename was not found`} );
  } 
}

module.exports = { createFile, getFiles, getFile, modifyFile, deleteFile };