const express = require('express');
const router = express.Router();

const { createFile, getFiles, getFile, modifyFile, deleteFile} = require('./fileControllers.js'); 

router.post('/', createFile);
router.get('/', getFiles);
router.get('/:filename', getFile);
router.put('/:filename', modifyFile);
router.delete('/:filename', deleteFile);
module.exports = router;