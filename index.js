const express = require('express');
const app = express();
const port = 8080;
const morgan = require('morgan');
const fileRoute = require('./fileRoutes.js');

app.use(express.json());
app.use(morgan('tiny'));
app.use('/api/files', fileRoute);

app.listen(port, () => {
  console.log(`Server is started at http://localhost:${port}`);
})
